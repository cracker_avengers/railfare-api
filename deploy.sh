#!/bin/bash

if [ "$CIRCLE_BRANCH" = "master" ] ; then
    ./openshift/oc login "$OC_URL" --token="$OC_TOKEN"
    ./openshift/oc import-image railfare-api:stable
fi;