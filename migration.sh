#!/bin/bash

./openshift/oc login "$1" --token="$2"
./resolve.sh
#secrets first
find ./openshift/bin -name "*sec.yaml" | xargs -L 1 ./openshift/oc create -f

#configmaps next
find ./openshift/bin/ -name "*cmap.yaml" | xargs -L 1 ./openshift/oc create -f

#imagestreams
find ./openshift/bin/ -name "*is.yaml" | xargs -L 1 ./openshift/oc create -f

#services
find ./openshift/bin/ -name "*svc.yaml" | xargs -L 1 ./openshift/oc create -f

#routes
find ./openshift/bin/ -name "*route.yaml" | xargs -L 1 ./openshift/oc create -f

#volume claims
find ./openshift/bin/ -name "*pvc.yaml" | xargs -L 1 ./openshift/oc create -f

#dc
find ./openshift/bin/ -name "*dc.yaml" | xargs -L 1 ./openshift/oc create -f