#!/bin/bash
set -oe pipefail

replace() {
    OLD=${1}
    FILE=${1/.secret/};
    FILE=${FILE/.tmp/};
    FILE=${FILE##*/}
    echo "$OLD" | xargs perl resolve.pl > "./openshift/bin/$FILE"
}

export -f replace

find ./openshift/ -maxdepth 2 -path ./openshift/bin -prune -o \
   -name "*.yaml*" -exec bash -c \
   'set -oe pipefail; for file ; do replace "$file" ; done' _ {} +
