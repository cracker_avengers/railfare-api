#!/bin/bash
set -oe pipefail

replace() {
    OLD=${1}
    FILE=${1/.secret/};
    FILE=${FILE/.tmp/};
    FILE=${FILE##*/}
    echo "$OLD" | xargs perl parse.pl > "./bin/$FILE"
}
export -f replace;
[[ -e "./env.sh" ]] && { find "$1" -name "*.yaml*" -exec bash -xc '. ./env.sh ; set -oe pipefail; for file ; do replace "$file" ; done' _ {} + ; } \
                  || { find "$1" -name "*.yaml*" -exec bash -xc 'set -oe pipefail; for file ; do replace "$file" ; done' _ {} + ; }
