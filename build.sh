#!/bin/bash

JAR_FILE=$(find target -name "*.jar");
VERSION="0.1";
NAME="blackjackj/railfare-api";
TAG="$VERSION.$CIRCLE_BUILD_NUM";
STABLE="stable";
[[ "dev" = "$CIRCLE_BRANCH" ]] && TAG="dev-$TAG";
IMG="$NAME:$TAG"
echo "TAG : ${TAG}"
docker build -q -t "$IMG" --build-arg JAR_FILE="${JAR_FILE}" .
[[ "master" = "$CIRCLE_BRANCH" ]] && docker tag "$IMG" "$NAME:$STABLE";
BUILD_STAT="$?"
if [[ "dev" = "$CIRCLE_BRANCH" || "master" = "$CIRCLE_BRANCH" ]] ; then
    echo "IMAGE: ${IMG}";
    docker login -u "$DOCKER_LOGIN" -p "$DOCKER_PASSWORD" && \
    { docker push "$IMG" ; [[ "master" = "$CIRCLE_BRANCH" ]] \
        && docker push "$NAME:$STABLE" || echo "dev: ok" ; };
else echo "Build status: $BUILD_STAT";
fi