#!/bin/bash

./openshift/oc login "$1" --token="$2"

./openshift/oc delete all --all
./openshift/oc delete secret --all
./openshift/oc delete pvc --all
./openshift/oc delete configmap --all