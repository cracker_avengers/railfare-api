package org.cracker_avengers.configs;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.Map;

@Configuration
@PropertySource(value = {"classpath:request.properties", "classpath:persistence.properties"})
@ConfigurationProperties(prefix = "request")
@Getter
@Setter
public class PropertyConfig {

    private Map<String, String> baseURL;

}
