package org.cracker_avengers.railfare.views;

public class TrainViews {

    public static interface TrainOnlyCode {}

    public static interface TrainWithId extends TrainOnlyCode {}

    public static interface TrainWithFLStops extends TrainWithId, StationViews.StationWithId, Views.ScheduleFullInfo {}

    public static interface TrainFullInfo extends TrainWithFLStops {}
}
