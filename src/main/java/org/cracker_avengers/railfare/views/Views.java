package org.cracker_avengers.railfare.views;

public class Views {

    public static interface ScheduleArrivalOnly {}

    public static interface ScheduleDepartureOnly {}

    public static interface ScheduleFullInfo {}

    public static interface YAllStationsRequest extends StationViews.StationWithCoords {}

}
