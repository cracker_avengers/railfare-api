package org.cracker_avengers.railfare.views;

public class StationViews {

    public static interface StationOnlyName {}

    public static interface StationWithId extends StationOnlyName {}

    public static interface StationWithCoords extends StationWithId {}

    public static interface StationFullInfo extends StationWithCoords {}
}
