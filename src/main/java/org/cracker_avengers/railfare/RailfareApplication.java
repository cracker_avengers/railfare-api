package org.cracker_avengers.railfare;

import org.cracker_avengers.configs.PropertyConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.core.env.Environment;

@SpringBootApplication
@Import({PropertyConfig.class})
public class RailfareApplication {

    @Autowired
    static Environment environment;

    public static void main(String[] args){
        //OverallStations_YRequest yr = new OverallStations_YRequest("localhost");
        //System.out.println(builder.buildRequest(yr));
        SpringApplication.run(RailfareApplication.class, args);
    }

}
