package org.cracker_avengers.railfare.api;

import org.cracker_avengers.railfare.yandex_schedule.YandexRequestBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")
public class RootAPI {

    @Autowired
    private YandexRequestBuilder builder;

    @GetMapping("/")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity ok(){
        try {
            builder.test();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity(HttpStatus.OK);
    }



}
