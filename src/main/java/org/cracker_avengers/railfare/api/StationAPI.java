package org.cracker_avengers.railfare.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.cracker_avengers.configs.PropertyConfig;
import org.cracker_avengers.railfare.entities.Station;
import org.cracker_avengers.railfare.services.impl.StationServiceImpl;
import org.cracker_avengers.railfare.views.StationViews;
import org.cracker_avengers.railfare.yandex_schedule.YandexClient;
import org.cracker_avengers.railfare.yandex_schedule.YandexException;
import org.cracker_avengers.railfare.yandex_schedule.requests.OverallStations_YRequest;
import org.cracker_avengers.railfare.yandex_schedule.response.OverallStations_YResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/stations")
public class StationAPI {

    @Autowired
    private YandexClient yandexClient;

    @Autowired
    private ObjectMapper mapper;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private StationServiceImpl stationService;

    @Autowired
    private PropertyConfig propertyConfig;

    @JsonView(StationViews.StationWithCoords.class)
    @GetMapping(path = "/{id}", produces = "application/json")
    public Station getById(@PathVariable long id) {
        return stationService.get(id).orElse(new Station());
    }

    @JsonView(StationViews.StationWithCoords.class)
    @GetMapping(path = "/", produces = "application/json")
    public List<Station> getByName(@RequestParam(name = "name") String name) {
        return stationService.getByCity(name);
    }

    @GetMapping(path = "/all", produces = "application/json")
    @JsonView(StationViews.StationWithCoords.class)
    @Transactional
    public List<Station> getAll(){

        try {
            List<Station> stations = mapper.readValue(
                    yandexClient.execute(
                            new OverallStations_YRequest()
                    ), OverallStations_YResponse.class
            ).getStations();
            int i = 1;
            for(Station s: stations){
                stationService.create(s);
                if(i%100 == 0){
                    entityManager.flush();
                    entityManager.clear();
                }
                i++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (YandexException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @JsonView(StationViews.StationWithCoords.class)
    @GetMapping(path = "/from/{id}", produces = "application/json")
    public List<Station> reachableFromStation(@PathVariable long id) {
        return stub();
    }

    private List<Station> stub(){
        List<Station> stations = new ArrayList<>();
        stations.add(new Station("Saint Petersburg", "SPB-1", 59, 30));
        stations.add(new Station("Hell", "666", 66, 13));
        stations.add(new Station("Haven", "7", 0, 0));
        return stations;
    }

}
