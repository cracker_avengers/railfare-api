package org.cracker_avengers.railfare.api;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.cracker_avengers.railfare.entities.Schedule;
import org.cracker_avengers.railfare.entities.Station;
import org.cracker_avengers.railfare.entities.StationTrain;
import org.cracker_avengers.railfare.entities.Train;
import org.cracker_avengers.railfare.services.impl.StationServiceImpl;
import org.cracker_avengers.railfare.services.impl.TrainServiceImpl;
import org.cracker_avengers.railfare.views.TrainViews;
import org.cracker_avengers.railfare.views.Views;
import org.cracker_avengers.railfare.yandex_schedule.YandexClient;
import org.cracker_avengers.railfare.yandex_schedule.YandexRequest;
import org.cracker_avengers.railfare.yandex_schedule.requests.StationsbyThread_YR;
import org.cracker_avengers.railfare.yandex_schedule.requests.ThreadsBetweenStations_YR;
import org.cracker_avengers.railfare.yandex_schedule.response.StationsbyThread_YResponse;
import org.cracker_avengers.railfare.yandex_schedule.response.ThreadsBetweenStations_YResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.*;

@RestController
@RequestMapping("/api/trains")
public class TrainAPI {

    @Autowired
    private YandexClient client;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private StationServiceImpl stationService;

    @Autowired
    private TrainServiceImpl trainService;

    @GetMapping
    public String hello(){
        return "Welcome";
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.OK)
    public void createTrain(){}

    @GetMapping(path = "/{id}", produces = "application/json")
    //@JsonView(TrainViews.TrainFullInfo.class)
    public StationsbyThread_YResponse getTrainById(@PathVariable String id){
        StationsbyThread_YR request = new StationsbyThread_YR(id);
        try {
            return mapper.readValue(client.execute(request), StationsbyThread_YResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new StationsbyThread_YResponse();
    }

    @DeleteMapping("/delete/{id}")
    public void deleteTrain(@PathVariable long id){}

    @GetMapping(value = "/search", produces = "application/json")
    @JsonView(TrainViews.TrainWithId.class)
    public List<Train> searchTrainbyStations(
            @RequestParam(name = "arr") String arr,
            @RequestParam(name = "dep") String dep,
            @RequestParam(name = "date", required = false) Optional<String> date){
        Station arrival = stationService.getFirstByCity(arr).orElse(new Station());
        System.out.println(arrival.getCityCode());
        Station departure = stationService.getFirstByCity(dep).orElse(new Station());
        System.out.println(departure.getCityCode());
        ThreadsBetweenStations_YR request = new ThreadsBetweenStations_YR(arrival.getCityCode(), departure.getCityCode());
        date.ifPresent(request::setDate);
        try {
            return mapper.readValue(client.execute(request),
                    ThreadsBetweenStations_YResponse.class).getTrains();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }



    private Train byIdStub(){
        Train reply = new Train();
        reply.setCode("109A");
        Station spb = new Station("Saint Petersburg", "SPB-1", 59, 30);
        Station sar = new Station("Saratov", "Saratov-1", 66, 66);
        Schedule strt = new Schedule(Instant.now(), Instant.now().plusSeconds(3600));
        Schedule stop = new Schedule(Instant.now().plusSeconds(86400), null);
        StationTrain s1 = new StationTrain();
        StationTrain s2 = new StationTrain();
        s1.setSchedule(strt);
        s2.setSchedule(stop);
        reply.addStationTrain(s1);
        reply.addStationTrain(s2);
        spb.addStationTrain(s1);
        sar.addStationTrain(s2);
        return reply;
    }

}
