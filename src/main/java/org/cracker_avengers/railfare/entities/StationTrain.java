package org.cracker_avengers.railfare.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class StationTrain {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne
    @JoinColumn(
            name = "station_id",
            foreignKey = @ForeignKey(name = "STATION_ID_KF")
    )
    @JsonBackReference(value = "station-station_train")
    private Station station;

    @ManyToOne
    @JoinColumn(
            name = "train_id",
            foreignKey = @ForeignKey(name = "TRAIN_ID_FK")
    )
    @JsonBackReference(value = "train-station_train")
    private Train train;

    @Embedded
    private Schedule schedule;

    public StationTrain(Station station, Train train, Schedule schedule) {
        setStation(station);
        setTrain(train);
        setSchedule(schedule);
    }
}
