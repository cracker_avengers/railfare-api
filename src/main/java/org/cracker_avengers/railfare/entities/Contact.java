package org.cracker_avengers.railfare.entities;

public interface Contact {

    void contact(String msg);
    String getContacts();

}
