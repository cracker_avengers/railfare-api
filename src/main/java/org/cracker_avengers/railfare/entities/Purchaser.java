package org.cracker_avengers.railfare.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Purchaser {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    private String username;

    @Transient
    private List<Contact> contacts;

    @OneToOne(mappedBy = "purchaser")
    private Cart cart;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "purchaser"
    )
    private List<Order> orders = new ArrayList<>();

    public Purchaser(String username){
        this.setUsername(username);
    }

    public void deleteCart(){
        this.cart.setPurchaser(null);
        this.cart = null;
    }

}
