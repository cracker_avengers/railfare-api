package org.cracker_avengers.railfare.entities;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.cracker_avengers.railfare.views.StationViews;
import org.cracker_avengers.railfare.views.TrainViews;
import org.cracker_avengers.railfare.views.Views;

import javax.persistence.*;
import javax.swing.text.View;
import java.util.*;
import java.util.stream.Collectors;

@Entity
@NoArgsConstructor
@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class Station {

    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    @JsonView({StationViews.StationWithId.class, TrainViews.TrainWithFLStops.class})
    private long id;

    @JsonView({StationViews.StationOnlyName.class, TrainViews.TrainWithFLStops.class})
    private String name;

    @JsonView(StationViews.StationWithCoords.class)
    private float lon;

    @JsonView(StationViews.StationWithCoords.class)
    private float lat;

    @JsonView(StationViews.StationOnlyName.class)
    private String city;

    private String cityCode;

    private String code;

    @OneToMany(
            mappedBy = "station",
            orphanRemoval = true
    )
    @JsonView(StationViews.StationFullInfo.class)
    @JsonManagedReference(value = "station-station_train")
    private Set<StationTrain> stationTrains = new HashSet<>();

    public void addStationTrain(StationTrain stationTrain){
        this.stationTrains.add(stationTrain);
        stationTrain.setStation(this);
    }

    public void removeStationTrain(StationTrain stationTrain){
        this.stationTrains.remove(stationTrain);
        stationTrain.setStation(null);
    }

    @OneToMany(mappedBy = "station")
    @JsonView({StationViews.StationFullInfo.class})
    private List<Provider> providers;

    public Station(String city, String name, float lon, float lat){
        setCity(city);
        setName(name);
        setLon(lon);
        setLat(lat);
    }

}
