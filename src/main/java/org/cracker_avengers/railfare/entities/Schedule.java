package org.cracker_avengers.railfare.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.cracker_avengers.railfare.views.Views;

import javax.persistence.*;
import java.time.Instant;

@NoArgsConstructor
@Getter
@Setter
@Embeddable
public class Schedule implements Comparable<Schedule>{

    @JsonView(Views.ScheduleFullInfo.class)
    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "dd-MM-yyyy hh:mm",
            timezone = "UTC"
    )
    private Instant arrival;

    @JsonView(Views.ScheduleFullInfo.class)
    @JsonFormat(
            shape = JsonFormat.Shape.STRING,
            pattern = "dd-MM-yyyy hh:mm",
            timezone = "UTC"
    )
    private Instant departure;


    public Schedule(Instant arrival, Instant departure){
        setArrival(arrival);
        setDeparture(departure);
    }

    @Override
    public int compareTo(Schedule schedule) {
        return this.arrival.compareTo(schedule.arrival);
    }
}