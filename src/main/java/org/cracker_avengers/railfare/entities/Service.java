package org.cracker_avengers.railfare.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.Duration;

@Entity
@Getter
@Setter
public class Service {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    private String name;

    private double cost;

    private Duration leadTime;

    @ManyToOne
    @JoinColumn(
            name = "provider_id",
            foreignKey = @ForeignKey(name = "PROVIDER_ID_FK")
    )
    private Provider provider;

    public Service(String name, double cost, Duration leadTime, Provider provider){
        this.setName(name);
        this.setCost(cost);
        this.setLeadTime(leadTime);
        this.provider = provider;
        this.provider.addService(this);
    }

}
