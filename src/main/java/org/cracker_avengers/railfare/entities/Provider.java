package org.cracker_avengers.railfare.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Provider {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    private String name;
    private String description;
    private Duration minDeliveryTime;
    private Duration maxDeliveryTime;
    private double minCost;

    @Transient
    private List<Contact> contacts = new ArrayList<>();

    @ManyToOne
    @JoinColumn(
            name = "station_id",
            foreignKey = @ForeignKey(name = "STATION_ID_FK")
    )
    private Station station;

    @OneToMany(mappedBy = "provider")
    private List<Service> services = new ArrayList<>();

    @OneToMany(mappedBy = "provider")
    private List<Order> orders = new ArrayList<>();

    public Provider(String name, Station linkedStation) {
        this.name = name;
        this.station = linkedStation;
    }

    public boolean changeContact(int n, Contact contact){
        if(n >= 0 && n < this.contacts.size()){
            this.contacts.set(n, contact);
            return true;
        }
        return false;
    }

    public boolean deleteContact(int n){
        if(this.contacts.size() == 1); //throw exception (need at least one contact)
        else{
            if(n < this.contacts.size()) {
                this.contacts.remove(n);
                return true;
            }
        }
        return false;
    }

    public boolean addContact(Contact contact){
        this.contacts.add(contact);
        return true;
    }

    public List<Order> getOrdersByStatus(Order.Status status){
        return this.getOrders().stream().filter(order -> order.getOrderStatus().equals(status)).collect(Collectors.toList());
    }

    public void addService(Service service){
        this.services.add(service);
        service.setProvider(this);
    }

    public void removeService(Service service){
        this.services.remove(service);
        service.setProvider(null);
    }

    public void addOrder(Order order){
        this.orders.add(order);
    }

    public boolean proceedOrder(Order order){
        if(this.orders.contains(order)){
            return order.proceedToNextStage();
        }
        return false;
    }

}
