package org.cracker_avengers.railfare.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.cracker_avengers.railfare.views.TrainViews;
import org.cracker_avengers.railfare.views.Views;

import javax.persistence.*;
import java.util.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Train {

    @JsonView(TrainViews.TrainWithId.class)
    @Id
    private String id;

    @JsonView(TrainViews.TrainOnlyCode.class)
    private String code;

    @JsonView(TrainViews.TrainWithId.class)
    private String name;

    @OneToMany(
            mappedBy = "train",
            orphanRemoval = true,
            cascade = CascadeType.REMOVE
    )
    @JsonManagedReference(value = "train-station_train")
    @JsonView(TrainViews.TrainFullInfo.class)
    private Set<StationTrain> stationTrains = new HashSet<>();

    public void addStationTrain(StationTrain stationTrain){
        this.stationTrains.add(stationTrain);
        stationTrain.setTrain(this);
    }

    public void removeStationTrain(StationTrain stationTrain){
        this.stationTrains.remove(stationTrain);
        stationTrain.setTrain(null);
    }

    @JsonProperty("arrival")
    @JsonView(TrainViews.TrainWithFLStops.class)
    public StationInfo arrivalStation(){
        StationTrain st = stationTrains.stream().min(new Comparator<StationTrain>() {
            @Override
            public int compare(StationTrain stationTrain, StationTrain t1) {
                return stationTrain.getSchedule().compareTo(t1.getSchedule());
            }
        }).orElse(new StationTrain());
        return new StationInfo(st.getStation(), st.getSchedule());
    }

    @JsonProperty("departure")
    @JsonView(TrainViews.TrainWithFLStops.class)
    public StationInfo departureStation(){
        StationTrain st = stationTrains.stream().max(new Comparator<StationTrain>() {
            @Override
            public int compare(StationTrain stationTrain, StationTrain t1) {
                return stationTrain.getSchedule().compareTo(t1.getSchedule());
            }
        }).orElse(new StationTrain());
        return new StationInfo(st.getStation(), st.getSchedule());
    }

    public Train(String code, Set<StationTrain> stationTrains){
        this.code = code;
        this.stationTrains = stationTrains;
    }

    class StationInfo {

        @Getter
        @JsonView(TrainViews.TrainWithFLStops.class)
        private Station station;

        @Getter
        @JsonView(TrainViews.TrainWithFLStops.class)
        private Schedule schedule;

        public StationInfo(Station s, Schedule schedule){
            this.station = s;
            this.schedule = schedule;
        }
    }
}
