package org.cracker_avengers.railfare.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Cart {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @OneToOne
    @JoinColumn(name = "purchaser_id")
    private Purchaser purchaser;

    @ElementCollection
    public List<OrderItem> orderItems = new ArrayList<>();

    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    private double totalSum;

    public Cart(Purchaser purchaser){
        this.purchaser = purchaser;
        this.clearCart();
    }

    public void clearCart(){
        for(Iterator<OrderItem> iterator = orderItems.iterator();
            iterator.hasNext();){
            OrderItem i = iterator.next();
            i.setCart(null);
            orderItems.remove(i);
        }
        this.totalSum = 0;
    }

    public void addOrderItem(OrderItem orderItem){
        this.orderItems.add(orderItem);
        orderItem.setCart(this);
    }

    public void removeOrderItem(OrderItem orderItem){
        this.orderItems.remove(orderItem);
        orderItem.setCart(null);
    }

    public double getTotalSum(){
        return this.orderItems.stream().mapToDouble(o -> {return o.getService().getCost()*o.getNumber();}).sum();
    }

}
