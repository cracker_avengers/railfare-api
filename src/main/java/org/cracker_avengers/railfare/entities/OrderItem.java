package org.cracker_avengers.railfare.entities;


import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@NoArgsConstructor
@Embeddable
public class OrderItem {

    @ManyToOne
    @JoinColumn(
            name = "service_id",
            foreignKey = @ForeignKey(name = "SERVICE_ID_FK")
    )
    private Service service;

    @ManyToOne
    @JoinColumn(
            name = "order_id",
            foreignKey = @ForeignKey(name = "ORDER_ID_FK")
    )
    private Cart cart;

    private int number;

}
