package org.cracker_avengers.railfare.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    @ManyToOne
    @JoinColumn(
            name = "purchaser_id",
            foreignKey = @ForeignKey(name = "PURCHASER_ID_FK")
    )
    private Purchaser purchaser;

    @ManyToOne
    @JoinColumn(
            name = "provider_id",
            foreignKey = @ForeignKey(name = "PROVIDER_ID_FK")
    )
    private Provider provider;

    @Enumerated(EnumType.STRING)
    private Status orderStatus;

    private int carriageNumber;

    private Duration leadTime;

    @OneToOne
    @JoinColumn(
            name = "cart_id",
            foreignKey = @ForeignKey(name = "CART_ID_FK")
    )
    private Cart cart;

    public boolean proceedToNextStage(){
        int n = this.getOrderStatus().ordinal();
        this.setOrderStatus(this.getOrderStatus().nextStatus());
        return n != this.getOrderStatus().ordinal();
    }

    public boolean cancelOrder(){
        this.setOrderStatus(Status.CANCELED);
        return true;
    }

    public enum Status {
        ACTIVE, EXECUTING, READY, DELIVERING, COMPLETED, CANCELED;
        private Status nextStatus(){
            switch (this){
                case CANCELED:{
                    return this; //throw exception (order canceled)
                }
                case COMPLETED:{
                    return this; //throw exception (order completed)
                }
                default:
                return Status.values()[this.ordinal()+1];
            }
        }
    }

}
