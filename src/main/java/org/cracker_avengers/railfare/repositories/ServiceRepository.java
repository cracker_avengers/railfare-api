package org.cracker_avengers.railfare.repositories;

import org.cracker_avengers.railfare.entities.Service;
import org.cracker_avengers.railfare.entities.Station;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ServiceRepository extends CrudRepository<Service, Long> {

}
