package org.cracker_avengers.railfare.repositories;

import org.cracker_avengers.railfare.entities.Cart;
import org.springframework.data.repository.CrudRepository;

public interface CartRepository extends CrudRepository<Cart, Long> {
}
