package org.cracker_avengers.railfare.repositories;

import org.cracker_avengers.railfare.entities.Train;
import org.springframework.data.repository.CrudRepository;

public interface TrainRepository extends CrudRepository<Train, Long> {
}
