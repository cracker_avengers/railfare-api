package org.cracker_avengers.railfare.repositories;

import org.cracker_avengers.railfare.entities.Purchaser;
import org.springframework.data.repository.CrudRepository;

public interface PurchaserRepository extends CrudRepository<Purchaser, Long> {
}
