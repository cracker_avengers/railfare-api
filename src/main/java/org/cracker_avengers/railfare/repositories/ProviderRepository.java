package org.cracker_avengers.railfare.repositories;

import org.cracker_avengers.railfare.entities.Provider;
import org.springframework.data.repository.CrudRepository;

public interface ProviderRepository extends CrudRepository<Provider, Long> {
}
