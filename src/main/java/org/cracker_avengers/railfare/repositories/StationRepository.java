package org.cracker_avengers.railfare.repositories;

import org.cracker_avengers.railfare.entities.Station;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface StationRepository extends CrudRepository<Station, Long> {

    List<Station> getAllByCity(String city);
    Optional<Station> getByCode(String code);
    List<Station> getAllByCityCode(String cityCode);
    Optional<Station> getFirstByCityAndCityCodeNotNull(String city);

}
