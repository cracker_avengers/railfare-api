package org.cracker_avengers.railfare.repositories;

import org.cracker_avengers.railfare.entities.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Long> {
}
