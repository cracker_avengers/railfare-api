package org.cracker_avengers.railfare.yandex_schedule;

import lombok.Setter;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class YandexClient implements YandexClientI {

    @Autowired
    private YandexRequestBuilder builder;

    @Override
    public String execute(YandexRequest yRequest) throws Exception {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(builder.buildRequest(yRequest)).build();
        try {
            Response response = client.newCall(request).execute();
            System.out.println(response.body());
            return response.body().string();
        }catch (IOException e){
            throw new YandexException(){

            };
        }
    }
}
