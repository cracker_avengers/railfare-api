package org.cracker_avengers.railfare.yandex_schedule;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.cracker_avengers.railfare.yandex_schedule.entities.YandexEntities;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Getter
@Setter
public abstract class YandexRequest {

    @YRequestParameter(name = "lang")
    protected YandexEntities.Lang lang = YandexEntities.Lang.ru_RU;

    @YRequestParameter(name = "format")
    protected YandexEntities.Format format = YandexEntities.Format.json;

    @YRequestParameter(name = "transport_types")
    protected YandexEntities.TransportType transportType = YandexEntities.TransportType.train;

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
    public @interface YRequestParameter {
        String name();
        String value() default "";
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.TYPE)
    public @interface YRequest {
        String[] exclude() default {};
        YRequestParameter[] include() default {};
        String baseURL();
    }
}
