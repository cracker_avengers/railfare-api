package org.cracker_avengers.railfare.yandex_schedule;

public interface YandexClientI {

    String execute(YandexRequest yRequest) throws Exception;

}
