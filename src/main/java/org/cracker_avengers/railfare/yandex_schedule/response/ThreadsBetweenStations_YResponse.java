package org.cracker_avengers.railfare.yandex_schedule.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.cracker_avengers.railfare.entities.StationTrain;
import org.cracker_avengers.railfare.entities.Train;
import org.cracker_avengers.railfare.yandex_schedule.YandexResponse;
import org.cracker_avengers.railfare.yandex_schedule.entities.YandexEntities;
import org.cracker_avengers.railfare.yandex_schedule.entities.YandexSchedlue;
import org.cracker_avengers.railfare.yandex_schedule.entities.YandexStation;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


@Getter
@Setter
@NoArgsConstructor
public class ThreadsBetweenStations_YResponse extends YandexResponse {

    private YandexStation from;
    private YandexStation to;
    private List<YandexEntities.Segment> segments;

    public static List<Train> getTrains(ThreadsBetweenStations_YResponse response){
        return response.getTrains();
    }

    public List<Train> getTrains(){
        return segments.stream().map(YandexEntities.Segment::getThread).map(thread -> {
            Train t = new Train();
            t.setId(thread.getUid());
            t.setName(thread.getTitle());
            t.setCode(thread.getNumber());
            return t;
        }).collect(Collectors.toList());
    }

}
