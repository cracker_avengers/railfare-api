package org.cracker_avengers.railfare.yandex_schedule.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.cracker_avengers.railfare.yandex_schedule.YandexResponse;
import org.cracker_avengers.railfare.yandex_schedule.entities.YandexStop;

import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class StationsbyThread_YResponse extends YandexResponse {

    private String uid;
    private String title;
    private String number;
    private List<YandexStop> stops;



}
