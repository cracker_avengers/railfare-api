package org.cracker_avengers.railfare.yandex_schedule.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import org.cracker_avengers.railfare.entities.Station;
import org.cracker_avengers.railfare.yandex_schedule.YandexResponse;
import org.cracker_avengers.railfare.yandex_schedule.entities.YandexEntities;
import org.cracker_avengers.railfare.yandex_schedule.entities.YandexStation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@JsonIgnoreProperties(value = {"pagination"}, ignoreUnknown = true)
public class OverallStations_YResponse extends YandexResponse {

    private List<YandexEntities.Country> countries;

    public static List<Station> getStations(OverallStations_YResponse response){
        return response.getStations();
    }

    public List<Station> getStations() {
        return countries.stream().filter(country -> "Россия".equals(country.getTitle()))
                .flatMap(c -> c.getRegions().stream())
                .flatMap(r -> r.getSettlements().stream()).map(settlement -> {
                    List<Station> stations= new ArrayList<>();
                    for(YandexStation yandexStation: settlement.getStations()){
                        if(!YandexEntities.TransportType.train.equals(yandexStation.getTransportType())) continue;
                        if(!YandexEntities.StationType.station.equals(yandexStation.getStationType()) &&
                            !YandexEntities.StationType.train_station.equals(yandexStation.getStationType())) continue;
                        Station station = new Station();
                        station.setCode(yandexStation.getCode());
                        station.setLat(yandexStation.getLatitude());
                        station.setLon(yandexStation.getLongitude());
                        station.setCity(settlement.getTitle());
                        station.setCityCode(settlement.getCodes().get("yandex_code"));
                        station.setName(yandexStation.getTitle());
                        stations.add(station);
                    }
                    return stations;
        }).flatMap(Collection::stream).collect(Collectors.toList());
    }


}
