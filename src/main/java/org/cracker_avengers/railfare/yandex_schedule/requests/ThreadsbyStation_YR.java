package org.cracker_avengers.railfare.yandex_schedule.requests;

import lombok.Getter;
import lombok.Setter;
import org.cracker_avengers.railfare.yandex_schedule.YandexRequest;
import org.springframework.beans.factory.annotation.Value;

@Getter
@Setter
@YandexRequest.YRequest(
        baseURL = "${request.baseURL.threads_by_station}"
)
public class ThreadsbyStation_YR extends YandexRequest {

    @YRequestParameter(name = "station")
    private String station;

    public ThreadsbyStation_YR(String s){
        this.station = s;
    }

}
