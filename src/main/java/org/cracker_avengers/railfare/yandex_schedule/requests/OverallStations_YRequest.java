package org.cracker_avengers.railfare.yandex_schedule.requests;

import lombok.Getter;
import lombok.Setter;
import org.cracker_avengers.railfare.yandex_schedule.YandexRequest;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Setter
@YandexRequest.YRequest(
        exclude = {"transport_types"},
        baseURL = "${request.baseURL.overall_stations}"
)
public class OverallStations_YRequest extends YandexRequest {}
