package org.cracker_avengers.railfare.yandex_schedule.requests;

import lombok.Getter;
import lombok.Setter;
import org.cracker_avengers.railfare.yandex_schedule.YandexRequest;
import org.cracker_avengers.railfare.yandex_schedule.entities.YandexEntities;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

@Setter
@Getter
@YandexRequest.YRequest(
        baseURL = "${request.baseURL.threads_between}"
)
public class ThreadsBetweenStations_YR extends YandexRequest {

    @YRequestParameter(name = "from")
    private String from;

    @YRequestParameter(name = "to")
    private String to;

    @YRequestParameter(name = "date", value = "")
    private String date = "";

    @YRequestParameter(name = "transport_types")
    private YandexEntities.TransportType type = YandexEntities.TransportType.train;

    public ThreadsBetweenStations_YR(String from, String to){
        this.from = from;
        this.to = to;
    }

    public ThreadsBetweenStations_YR setDate(String date){
        this.date = date;
        return  this;
    }

}
