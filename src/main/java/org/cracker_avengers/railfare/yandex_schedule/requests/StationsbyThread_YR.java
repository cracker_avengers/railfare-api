package org.cracker_avengers.railfare.yandex_schedule.requests;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.cracker_avengers.railfare.yandex_schedule.YandexRequest;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Setter
@NoArgsConstructor
@YandexRequest.YRequest(
        baseURL = "${request.baseURL.stations_by_thread}"
)
public class StationsbyThread_YR extends YandexRequest {

    @YRequestParameter(name = "uid")
    private String uid;

    public StationsbyThread_YR(String uid){
        this.uid = uid;
    }

}
