package org.cracker_avengers.railfare.yandex_schedule.entities;

import com.fasterxml.jackson.annotation.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class YandexStation {

    @JsonIgnore
    @Getter(AccessLevel.NONE)
    private Map<String, String> codes = new HashMap<>();

    @JsonProperty("transport_type")
    private YandexEntities.TransportType transportType;

    @JsonProperty("station_type")
    private YandexEntities.StationType stationType;

    @JsonIgnore
    @Getter(AccessLevel.NONE)
    private String title;

    @JsonIgnore
    private float longitude;
    @JsonIgnore
    private float latitude;

    @JsonProperty("codes")
    public void setCodes(Map<String, String> codes){this.codes = codes;}

    @JsonSetter("code")
    public void setCode(String value){
        this.codes.put("yandex_code", value);
    }

    @JsonGetter("code")
    public String getCode() {return this.codes.get("yandex_code");}

    @JsonSetter("title")
    public void setTitle(String title){this.title = title;}

    @JsonGetter("name")
    public String getTitle(){return this.title;}

    @JsonSetter("latitude")
    public void setLatitude(float latitude){this.latitude = latitude;}

    @JsonSetter("longitude")
    public void setLongitude(float longitude) {this.longitude = longitude;}

}
