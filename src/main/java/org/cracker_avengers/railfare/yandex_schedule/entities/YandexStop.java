package org.cracker_avengers.railfare.yandex_schedule.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class YandexStop {

    private String arrival;
    private String departure;
    private YandexStation station;

}
