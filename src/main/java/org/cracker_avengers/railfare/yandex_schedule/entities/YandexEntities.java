package org.cracker_avengers.railfare.yandex_schedule.entities;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class YandexEntities {

    public enum Lang {
        ru_RU;
    }

    public enum Format {
        json, xml;
    }

    public enum TransportType {
        @JsonProperty("train") train,
        @JsonProperty("plane") plane,
        @JsonProperty("suburban") suburban,
        @JsonProperty("bus") bus,
        @JsonProperty("water") water,
        @JsonProperty("helicopter") helicopter,
        @JsonProperty("pseudo-gortrans") pseudo_gortrans,
        @JsonProperty("sea") sea;
    }

    public enum StationType {
        @JsonProperty("station") station,
        @JsonProperty("platform") platform,
        @JsonProperty("stop") stop,
        @JsonProperty("checkpoint") checkpoint,
        @JsonProperty("post") post,
        @JsonProperty("crossing") crossing,
        @JsonProperty("overtaking_point") overtaking_point,
        @JsonProperty("airport") airport,
        @JsonProperty("bus_station") bus_station,
        @JsonProperty("bus_stop") bus_stop,
        @JsonProperty("unknown") unknown,
        @JsonProperty("port") port,
        @JsonProperty("port_point") port_point,
        @JsonProperty("wharf") wharf,
        @JsonProperty("river_port") river_port,
        @JsonProperty("marine_station") marine_station,
        @JsonProperty("train_station") train_station,
        @JsonProperty("") @JsonInclude() misc;

        @JsonCreator
        public static StationType forName(String name){
            for(StationType type : StationType.values()){
                if(type.name().equals(name)) return type;
            }
            return StationType.misc;
        }

    }


    @Getter
    @Setter
    @NoArgsConstructor
    public static class Country {
        private Map<String, String> codes;
        private String title;
        private List<Region> regions;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    public static class Region {
        private Map<String, String> codes;
        private String title;
        private List<Settlement> settlements;
    }

    @Getter
    @Setter
    @NoArgsConstructor
    public static class Settlement {

        private Map<String, String> codes;
        private String title;
        private List<YandexStation> stations;

    }

    @Getter
    @Setter
    @NoArgsConstructor
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Segment {

        private YandexStation from;
        private YandexStation to;

        private String arrival;
        private String departure;
        private YandexThread thread;

    }

}
