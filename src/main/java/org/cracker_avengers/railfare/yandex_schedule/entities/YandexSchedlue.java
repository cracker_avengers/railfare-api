package org.cracker_avengers.railfare.yandex_schedule.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class YandexSchedlue {

    private Date arrival;
    private Date departure;
    private YandexThread thread;

}
