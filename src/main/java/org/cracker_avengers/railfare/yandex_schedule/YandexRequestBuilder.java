package org.cracker_avengers.railfare.yandex_schedule;

import lombok.Setter;
import org.cracker_avengers.railfare.yandex_schedule.requests.OverallStations_YRequest;
import org.cracker_avengers.railfare.yandex_schedule.requests.ThreadsBetweenStations_YR;
import org.cracker_avengers.railfare.yandex_schedule.requests.ThreadsbyStation_YR;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Component
public class YandexRequestBuilder {

    @Autowired
    private Environment environment;

    @Setter
    @Value("${request.token}")
    private String apiKey;

    private Map<String, Byte> exclude = new HashMap<>();
    private Map<String, String> params = new HashMap<>();
    private StringBuilder builder;

    public <T extends YandexRequest>  String buildRequest(T request) throws Exception {
        if(!request.getClass().isAnnotationPresent(YandexRequest.YRequest.class)) throw new Exception("No annotation found");
        reset();
        params.put("apikey", apiKey);
        YandexRequest.YRequest yRequest = request.getClass().getAnnotation(YandexRequest.YRequest.class);
        excludeParams(yRequest);
        this.builder = new StringBuilder();
        Class t = request.getClass();
        while(YandexRequest.class.isAssignableFrom(t)){
            resolveParams(t, request);
            t = t.getSuperclass();
        }
        includeParams(yRequest);
        this.stringifyParams();
        this.getBaseURL(yRequest);
        System.out.println(this.builder);
        return builder.toString();
    }

    private void reset(){
        this.exclude = new HashMap<>();
        this.params = new HashMap<>();
        this.builder = new StringBuilder();
    }

    private <T extends YandexRequest> void resolveParams(Class<? extends YandexRequest> t, T request) throws IllegalAccessException {
        String tmp;
        for(Field field: t.getDeclaredFields()){
            if (field.isAnnotationPresent(YandexRequest.YRequestParameter.class)) {
                YandexRequest.YRequestParameter parameter = field.getAnnotation(YandexRequest.YRequestParameter.class);
                if(exclude.containsKey(parameter.name())) continue;
                field.setAccessible(true);
                try {
                    tmp = environment.resolveRequiredPlaceholders(field.get(request).toString());
                } catch (IllegalAccessException e) {
                    tmp = field.get(request).toString();
                }
                this.params.put(parameter.name(), tmp);
            }
        }
    }

    private void includeParams(YandexRequest.YRequest request){
        for(YandexRequest.YRequestParameter a: request.include()){
            params.put(a.name(), a.value());
        }
    }

    private void excludeParams(YandexRequest.YRequest request){
        for(String s: request.exclude()){
            exclude.put(s, null);
        }
    }

    private void stringifyParams(){
        Map.Entry<String, String> param;
        for(Iterator<Map.Entry<String, String>> iterator = params.entrySet().iterator();
            iterator.hasNext();){
            param = iterator.next();
            builder.append(String.format("%s=%s", param.getKey(), iterator.hasNext() ? param.getValue()+"&" : param.getValue()));
        }
    }

    private void getBaseURL(YandexRequest.YRequest request){
        String tmp;
        try{
            tmp = environment.resolveRequiredPlaceholders(request.baseURL());
        }catch (IllegalArgumentException e){
            tmp = request.baseURL();
        }
        builder.insert(0, String.format("%s/?", tmp));
    }

    public void test() throws Exception {
        System.out.println(buildRequest(new OverallStations_YRequest()));
        System.out.println(buildRequest(new ThreadsBetweenStations_YR("c146", "c213")));
        System.out.println(buildRequest(new ThreadsbyStation_YR("s9623135")));
    }

}
