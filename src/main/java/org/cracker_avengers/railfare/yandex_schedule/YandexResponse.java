package org.cracker_avengers.railfare.yandex_schedule;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
public abstract class YandexResponse {

    @JsonProperty("pagination")
    private Pagination pagination;

    @Getter
    @Setter
    @NoArgsConstructor
    public static class Pagination {

        private int total;
        private int limit;
        private int offset;

    }

}
