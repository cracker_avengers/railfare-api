package org.cracker_avengers.railfare.services.impl;

import org.cracker_avengers.railfare.entities.Train;
import org.cracker_avengers.railfare.repositories.TrainRepository;
import org.cracker_avengers.railfare.services.TrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class TrainServiceImpl implements TrainService {

    @Autowired
    private TrainRepository repository;

    @Override
    public Train create(Train train) {
        return repository.save(train);
    }

    @Override
    public Optional<Train> get(long id) {
        return repository.findById(id);
    }

    @Override
    public Train update(Train train) {
        return repository.save(train);
    }

    @Override
    public void delete(long id) {
        repository.deleteById(id);
    }
}
