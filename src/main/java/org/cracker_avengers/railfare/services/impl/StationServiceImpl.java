package org.cracker_avengers.railfare.services.impl;

import org.cracker_avengers.railfare.entities.Station;
import org.cracker_avengers.railfare.repositories.StationRepository;
import org.cracker_avengers.railfare.services.StationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StationServiceImpl implements StationService {

    @Autowired
    private StationRepository repository;

    @Override
    public Station create(Station station) {
        return repository.save(station);
    }

    @Override
    public Optional<Station> get(long id) {
        return repository.findById(id);
    }

    @Override
    public List<Station> getByCity(String city) {
        return repository.getAllByCity(city);
    }

    @Override
    public Station update(Station station) {
        return repository.save(station);
    }

    @Override
    public Optional<Station> getByCode(String code) {
        return repository.getByCode(code);
    }

    @Override
    public List<Station> getByCityCode(String cityCode) {
        return repository.getAllByCity(cityCode);
    }

    @Override
    public Optional<Station> getFirstByCity(String city) {
        return repository.getFirstByCityAndCityCodeNotNull(city);
    }

    @Override
    public void delete(long id) {
        repository.deleteById(id);
    }
}
