package org.cracker_avengers.railfare.services;

import org.cracker_avengers.railfare.entities.Train;

import java.util.Optional;

public interface TrainService {

    Train create(Train train);
    Optional<Train> get(long id);
    Train update(Train train);
    void delete(long id);

}
