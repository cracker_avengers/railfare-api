package org.cracker_avengers.railfare.services;

import org.cracker_avengers.railfare.entities.Station;

import java.util.List;
import java.util.Optional;

public interface StationService {

    Station create(Station station);
    Optional<Station> get(long id);
    List<Station> getByCity(String city);
    Station update(Station station);
    Optional<Station> getByCode(String code);
    List<Station> getByCityCode(String cityCode);
    Optional<Station> getFirstByCity(String city);
    void delete(long id);

}
