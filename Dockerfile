FROM openjdk:8-jdk-alpine
ARG JAR_FILE
COPY ${JAR_FILE} app.jar
EXPOSE 8090/tcp
ENTRYPOINT ["java", "-Xmx512m", "-XX:+UnlockExperimentalVMOptions",  "-XX:+UseCGroupMemoryLimitForHeap", "-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]